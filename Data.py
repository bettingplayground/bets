import sqlite3
from datetime import datetime
class DataStore:
 
    # default constructor
    def __init__(self):
        self.conn = sqlite3.connect(':memory:',check_same_thread=False)
 
    def setup_db(self):
        cur = self.conn.cursor()
        # Create table
        cur.execute('''CREATE TABLE stocks
               (date text, trans text, symbol text, qty real, price real)''')
        cur.execute('''CREATE TABLE rates
               (date text, name text, sport text, vendor text, rate1 real, rate2 real)''')
        cur.execute('''CREATE TABLE rates_history
               (ID INTEGER PRIMARY KEY   AUTOINCREMENT, date text, name text, sport text, vendor text, rate1 real, rate2 real)''')
        # Insert a row of data
        cur.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")

        #Save (commit) the changes
        self.conn.commit()

    # Game object    
    def addRatesToDb(self, game):
        cur = self.conn.cursor()
        select_cmd = """SELECT date, rate1, rate2 FROM rates 
                        WHERE name='{}' AND sport='{}' AND vendor='{}' 
                        ORDER BY date DESC LIMIT 1""".format(game.name, game.sport, game.vendor)
        results = cur.execute(select_cmd).fetchall()
        if len(results) == 0:
            cur.execute("INSERT INTO rates VALUES ('{0}','{1}','{2}','{3}',{4},{5})".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S") ,game.name,game.sport, game.vendor, game.rate1,game.rate2))
        else:
            for row in results:
                #print(row)
                if game.rate1 != row[1] or game.rate2 != row[2]:
                    cur.execute("""INSERT INTO rates_history(date, name, sport, vendor, rate1, rate2) 
                                    VALUES ('{0}','{1}','{2}','{3}',{4},{5})""".format(row[0] ,game.name,game.sport, game.vendor, row[1],row[2]))
                # Always update rates table
                cur.execute("""UPDATE rates SET date='{0}', rate1={1}, rate2={2}
                                WHERE name='{3}' AND sport='{4}' AND vendor='{5}' 
                                """.format(datetime.now().strftime("%d/%m/%Y %H:%M:%S"), game.rate1, game.rate2,game.name, game.sport, game.vendor))

        self.conn.commit()

    def read_rates(self):
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM rates")
        return  cur.fetchall()

    def read_rates_history(self):
        cur = self.conn.cursor()
        self.clean_old_data_history(cur)
        
        cur.execute("SELECT * FROM rates_history")
        return  cur.fetchall()


    def clean_old_data_history(self, cursor):
        records_to_keep = 5
        clean_sql = """SELECT rh.name || rh.sport || rh.vendor, max(ID), count(1) FROM rates_history rh
        
        GROUP BY rh.name || rh.sport || rh.vendor
        HAVING count(1) >{0}
        """.format(records_to_keep)
        
        for row in cursor.execute(clean_sql): 
            del_sql = """DELETE FROM rates_history 
                         WHERE ID IN (SELECT ID FROM rates_history rh WHERE rh.name || rh.sport || rh.vendor = '{0}'
                         ORDER BY ID DESC
                         LIMIT 10000 OFFSET {1}) 
                         """.format(row[0], records_to_keep)
            cursor.execute(del_sql)
        self.conn.commit()
        return True


    # a method for printing data members
    def print_conn(self):
        print(self.conn)
        cur = self.conn.cursor()
        for row in cur.execute('SELECT * FROM stocks ORDER BY price'):
            print(row)

 
