class Game:
  def __init__(self, name, vendor, rate1, rate2, sport):
    self.name = name
    self.rate1 = rate1
    self.rate2 = rate2
    self.vendor = vendor
    self.sport = sport

  def get_printable(self):
    return self.name+"/"+self.vendor

