import time
import Data
import json
from flask import Flask, render_template
import threading
from Game import Game


import asyncio
from websockets import serve

TCP_IP = '127.0.0.1'
TCP_PORT = 8888
BUFFER_SIZE = 1024
app = Flask(__name__)
db = None


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/games')
def render_games():
    return render_template('rates_template.html', data=db.read_rates(), data_h =db.read_rates_history() )


def parseRates(game):
    g = Game(game["name"], game["vendor"], game["r1"], game["r2"], game["sport"])
    return g



async def echo(websocket):
    async for message in websocket:
        
        try:
            parsed_msg = json.loads(message)
            game = parseRates(parsed_msg)
            #print(game.get_printable())
            db.addRatesToDb(game)


        except Exception as e:
            print(repr(e))
        await websocket.send(message)

async def main():

    print('Hello')
    
    # creating object of the class
    global db
    db = Data.DataStore()
    db.setup_db()
    db.print_conn()
    
    try:
        print('ok')
        time.sleep(2.2)
        async with serve(echo, "localhost", 8888):
            await asyncio.Future()  # run forever
       
    except KeyboardInterrupt:
            print('Exiting...')
            quit()

       

if __name__ == "__main__":
    threading.Thread(target=lambda: app.run(host="localhost", port=5000, debug=True, use_reloader=False)).start()
    asyncio.run(main())
    